import React from "react";
import "./banner.css";

interface BannerProps {
	children?: any;
	title?: string;
}

const Banner = ({ children, title }: BannerProps) => {
	return <div className="banner">{children}</div>;
};

export default Banner;
