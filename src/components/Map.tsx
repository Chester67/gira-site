import GoogleMapReact from "google-map-react";
import { LocationIcon } from "./LocationIcon";

export const Map = () => {
	const defaultProps = {
		center: {
			lat: 48.87,
			lng: 2.33,
		},
		zoom: 11,
	};
	const location = {
		lat: 48.87,
		lng: 2.33,
	};

	return (
		<div
			style={{
				height: "60vh",
				width: "100%",
				marginLeft: "auto",
			}}
		>
			<GoogleMapReact
				bootstrapURLKeys={{
					key: "AIzaSyDiCLlolpsHiVKjqPhdv4BFuW1qt-twtlY",
				}}
				defaultCenter={defaultProps.center}
				defaultZoom={defaultProps.zoom}
			>
				<LocationIcon lat={location.lat} lng={location.lng} />
			</GoogleMapReact>
		</div>
	);
};
