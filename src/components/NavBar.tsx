import React from "react";
import { Navbar, Nav, Image, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
import { removeItem } from "../utils/localStorage";
import profil from "../assets/img/profil/profil.png";

interface IProps {}
interface IState {
	expanded: boolean;
}
export default class NavBar extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			expanded: false,
		};
	}

	setExpanded = (exp: boolean) => {
		this.setState({ expanded: exp });
	};

	logoutUser = () => {
		console.log("logout is clicked");
		removeItem("user");
	};

	render() {
		const { expanded } = this.state;
		return (
			<Navbar className="center" collapseOnSelect expanded={expanded} expand="lg">
				<Navbar.Brand href="#home">
					<Image className="logo" src=""></Image>
				</Navbar.Brand>
				<Navbar.Toggle
					onClick={() => this.setExpanded(!expanded)}
					aria-controls="responsive-navbar-nav"
				/>
				<Navbar.Collapse id="responsive-navbar-nav">
					<Nav className="mr-auto"></Nav>
					<Nav onClick={() => this.setExpanded(false)}>
						<div className="navbar-vl d-none d-sm-none d-md-none d-lg-block d-xl-block" />
						<Link className="nav-link txt-cent" to="/produits">
							produits
						</Link>
						<div className="navbar-vl d-none d-sm-none d-md-none d-lg-block d-xl-block separator-cent" />
						<Link className="nav-link txt-cent" to="/magasins">
							magasins
						</Link>
						<div className="navbar-vl d-none d-sm-none d-md-none d-lg-block d-xl-block separator-cent" />
						<Link className="nav-link txt-cent" to="/consommation">
							consommation
						</Link>
						<div className="navbar-vl d-none d-sm-none d-md-none d-lg-block d-xl-block separator-cent" />
						<NavDropdown
							title={
								<span>
									<img src={profil} alt="" height="50" width="50" />{" "}
									Profil
								</span>
							}
							className="bg-drop"
							id="basic-nav-dropdown"
						>
							<NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
							<NavDropdown.Item href="#action/3.2">
								Another action
							</NavDropdown.Item>
							<NavDropdown.Item href="#action/3.3">
								Something
							</NavDropdown.Item>
							<NavDropdown.Divider />
							<NavDropdown.Item onClick={this.logoutUser}>
								Déconnexion
							</NavDropdown.Item>
						</NavDropdown>
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		);
	}
}
