import React from "react";

export const ErrorMessage = ({ message = "" }) => {
	return <small className="form-text">{message}</small>;
};
