export const ContactUs = () => {
	return (
		<div>
			<div className="contact">
				<h4>Nous contacter</h4>
				<div>Email : rh@giraconsulting.fr / Tel : 06 43 37 18 54 </div>
			</div>
			<div className="contact">
				<h4>Adresse</h4>
				<div>
					16-18 rue de Londres <br />
					75009 Paris <br />
				</div>
			</div>
		</div>
	);
};
