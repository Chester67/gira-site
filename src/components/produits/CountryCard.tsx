/* eslint-disable jsx-a11y/alt-text */
import React, { Fragment } from "react";
import PropTypes from "prop-types";

class CountryCard extends React.Component<any, any> {
	static propTypes: {
		country: PropTypes.Validator<
			PropTypes.InferProps<{
				code: PropTypes.Validator<string>;
				nom: PropTypes.Validator<string>;
			}>
		>;
	};

	render() {
		const {
			code = "",
			nom = "",
			image = "",
			marque = "",
			comparateurs = [],
		} = this.props.country || {};

		return (
			<Fragment>
				<div className="col-sm-6 col-md-3 country-card pl-0 pr-0">
					<div className="country-card-container mx-2 my-3 d-flex flex-column align-items-center p-0">
						<div className="position-relative px-2 rounded-left">
							<img
								className="produit-image"
								src={`https://supercompareur.fr/imageproduit/${image}`}
							/>
						</div>
						<div className="country-text-container">
							<span className="country-region text-secondary text-uppercase">
								{code}
							</span>
							<span className="country-name text-dark d-block font-weight-bold">
								{nom}
							</span>
							<span className="country-name text-dark d-block font-weight-bold">
								{marque}
							</span>
							{comparateurs.map((item: any) => (
								<div className="country-list d-flex flex-row spacing-manage">
									<span className="country-price">
										{item.magasin.distributeur.nom}:
									</span>
									<span className="country-price font-weight-bold">
										{" "}
										{item.prix}€
									</span>
								</div>
							))}
						</div>
					</div>
				</div>
			</Fragment>
		);
	}
}

CountryCard.propTypes = {
	country: PropTypes.shape({
		code: PropTypes.string.isRequired,
		nom: PropTypes.string.isRequired,
		image: PropTypes.string.isRequired,
		marque: PropTypes.string.isRequired,
	}).isRequired,
};

export default CountryCard;
