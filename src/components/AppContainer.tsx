import React from "react";

interface ContainerProps {
	className: string;
	children: any;
}

export const AppContainer = ({ className, children }: ContainerProps) => {
	return <div className={className || "default-container"}>{children}</div>;
};
