import React from "react";
import Modal from "react-bootstrap/Modal";
import "./AppModal.css";

interface AppModalProps {
	show: boolean;
	onHide?: Function;
	title?: string;
	children: any;
	dialogClassName?: string;
}

export const AppModal = (props: AppModalProps) => {
	return (
		<Modal
			show={props.show}
			onHide={props.onHide}
			dialogClassName={props.dialogClassName}
		>
			<Modal.Header closeButton>
				<Modal.Title>{props.title}</Modal.Title>
			</Modal.Header>
			<Modal.Body>{props.children}</Modal.Body>
		</Modal>
	);
};
