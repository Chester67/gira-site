import { Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";
import { RouteProps } from "react-router";
import { RootState } from "../../store";
import history from "./history";
import { getStore } from "../../utils/localStorage";

interface Props extends RouteProps {
	login: any;
}

class AuthorizedRouteHOC extends Route<Props> {
	componentDidMount() {
		//const { response } = this.props.login;
		const user = getStore("user");
		console.log("token is => " + JSON.stringify(user));
		if (!user) {
			console.log("no token now");
			history.push("/");
		}
	}
}

function mapStateToProps(states: RootState) {
	return {
		login: states.login.result,
	};
}

export default connect(mapStateToProps)(AuthorizedRouteHOC);
