export const TechnoLogo = ({ img, alt }: { img: any; alt: string }) => {
	return (
		<div className="col-6 col-xl-2 col-md-3 col-lg-2 col-xs-6 col-sm-6 goal-line">
			<img src={img} className="techno-img-responsive" alt="alt" />
		</div>
	);
};
