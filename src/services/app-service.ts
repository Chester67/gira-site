const getOffers = () => {
	return fetch("offres.json", {
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
	}).then((res) => res.json());
}; 

const getTechniqueOffers = () => {
	return fetch("offres/technique.json", {
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
	}).then((res) => res.json());
}; 

const getFonctionnelOffers = () => {
	return fetch("offres/fonctionnel.json", {
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
	}).then((res) => res.json());
}; 

const getManagmentOffers = () => {
	return fetch("offres/managment.json", {
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
	}).then((res) => res.json());
}; 

const getActus = () => {
	return fetch("actus.json", {
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
	}).then((res) => res.json());
};

export const AppService = {
	getOffers,
	getTechniqueOffers,
	getFonctionnelOffers,
	getManagmentOffers,
	getActus
};
