import { PRODUITS_API, PRODUITS_TYPES_API } from "../utils/apiUrls";

const apiProduitsPagination = async (currentPage: number, pageLimit: number) => {
	try {
    const url = PRODUITS_API + `?page=${currentPage}&limit=${pageLimit}`
    const response = await fetch(url);
    return response.json();
  } catch (error) {
    throw new Error('Exception');
  }
}; 

const apiTypeProduits = async (categorie: number) => {
	try {
    const url = PRODUITS_TYPES_API + `?categorie=${categorie}`
    const response = await fetch(url);
    return response.json();
  } catch (error) {
    throw new Error('Exception');
  }
}; 

export const ProduitService = {
	apiProduitsPagination,
  apiTypeProduits,
};