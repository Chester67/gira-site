import * as React from "react";
import { connect } from "react-redux";

interface State {}

interface Props {}

class Magasins extends React.Component<Props, State> {
	constructor(prop: Props) {
		super(prop);
		this.state = {};
	}

	//CYCLE DE VIE----------------------------
	componentDidUpdate(prevProps: Props, prevState: State) {}

	render() {
		return (
			<div className="container">
				<div className="container">
					<div className="row col-12">
						<div
							className="col-6 country-card-container"
							style={{ height: 200 }}
						>
							Regular col-sm-4
						</div>
						<div className="col-6 d-flex flex-column">
							<div
								className="col country-card-container"
								style={{ height: 95 }}
							>
								Flex item inside col-sm-8
							</div>
							<div
								className="col country-card-container"
								style={{ marginTop: 10, height: 95 }}
							>
								Flex item inside col-sm-8
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
const mapStateToProps = (states: any, ownProps: any): any => {
	return {
		accessToken: states.session.accessToken,
	};
};

// const mapDispatchToProps = (
// 	dispatch: ThunkDispatch<{}, {}, any>,
// 	ownProps: OwnProps
// ): DispatchProps => {
// 	return {
// 		login: async (username, password) => {
// 			await dispatch(login(username, password));
// 			console.log("Login completed [UI]");
// 		},
// 	};
// };

export default connect(mapStateToProps)(Magasins);
