import * as React from "react";
import { RootState } from "../../store";
import { connect } from "react-redux";
import { loginAction } from "../../store/login/actions";
import { LoginResult } from "../../store/login/reducers";
import { setStore } from "../../utils/localStorage";
import { RouteComponentProps } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import "./Login.css";

interface State {
	email: any;
	password: any;
	errors: {
		email: any;
		password: any;
	};
	loginStatus: any;
	submitted: boolean;
	testResp: any;
	[key: string]: any;
}

interface OwnProps {
	history: RouteComponentProps["history"];
	location: RouteComponentProps["location"];
	match: RouteComponentProps["match"];
}

interface DispatchProps {
	loginAction: (email: string, password: string) => void;
	//fetchLoginSuccess: (test: any) => void;
}

interface StateProps {
	login: LoginResult;
}

type Props = StateProps & OwnProps & DispatchProps;

class Login extends React.Component<Props, State> {
	constructor(prop: Props) {
		super(prop);
		this.state = {
			email: "",
			password: "",
			errors: {
				email: "Email obligatoire!",
				password: "Mot de passe obligatoire!",
			},
			loginStatus: "Chargement...",
			submitted: false,
			testResp: null,
		};
	}

	//CYCLE DE VIE----------------------------
	componentDidUpdate(prevProps: Props, prevState: State) {
		const { response } = this.props.login;
		console.log("response here 1 ===> " + JSON.stringify(response));
		if (response && prevProps.login.response !== response) {
			this.manageResponse(response);
		}
	}

	static getDerivedStateFromProps(props: Props, state: State) {
		console.log("response here 0 ===> " + JSON.stringify(props.login.response));
		if (props.login.response !== state.testResp) {
			return {
				testResp: props.login.response,
			};
		}
		return null;
	}

	//METHODES---------------------------------
	inputChange = (event: any) => {
		const { name, value } = event.target;
		this.setState({ [name]: value });
		this.validationErrorMessage(event);
	};

	validationErrorMessage = (event: any) => {
		const { name, value } = event.target;
		let errors = this.state.errors;
		switch (name) {
			case "email":
				errors.email = value.length < 1 ? "Enter User Name" : "";
				break;
			case "password":
				errors.password = value.length < 1 ? "Enter Password" : "";
				break;
			default:
				break;
		}
		this.setState({ errors });
	};

	validateForm = (errors: any) => {
		let valid = true;
		console.log(errors);
		Object.entries(errors).forEach((item: any) => {
			console.log(item);
			item && item[1].length > 0 && (valid = false);
		});
		console.log(valid);
		return valid;
	};

	loginForm = async (event: any) => {
		this.setState({ submitted: true });
		event.preventDefault();
		if (this.validateForm(this.state.errors)) {
			console.info("Valid Form => " + this.state.email + "-" + this.state.password);
			this.props.loginAction(this.state.email, this.state.password);
		} else {
			console.log("Invalid Form");
		}
	};

	manageResponse = (response: any) => {
		console.log("response here 2 ===> " + JSON.stringify(response));
		if (response.error) {
			alert("Invalid User Details!");
		} else if (response.token) {
			setStore("user", this.props.login.response);
			this.props.history.push("/admin/magasins");
		}
	};

	render() {
		const { email, password, errors, submitted, loginStatus } = this.state;
		const { response } = this.props.login;
		console.log("response here 3 ===> " + JSON.stringify(response));
		return (
			<div className="container d-flex w-100">
				<div className="card margin-top-login">
					<div className="card-header bg-transparent border-0 text-center text-uppercase">
						<h5>Authentification</h5>
					</div>
					<div className="card-body">
						<form className="mb-4">
							<div className="row justify-content-center">
								<div className="mb-2">
									<input
										type="text"
										value={email}
										name="email"
										onChange={(e) => {
											this.inputChange(e);
										}}
										className="form-control"
										id="email"
										placeholder="Email"
									/>
									{submitted && errors.email.length > 0 && (
										<span className="error login-error">
											{errors.email}
										</span>
									)}
								</div>
							</div>
							<div className="row justify-content-center">
								<div className="mb-2">
									<input
										type="password"
										value={password}
										autoComplete="on"
										name="password"
										onChange={(e) => {
											this.inputChange(e);
										}}
										className="form-control"
										id="password"
										placeholder="Password"
									/>
									{submitted && errors.password.length > 0 && (
										<span className="error login-error">
											{errors.password}
										</span>
									)}
								</div>
							</div>
							<div className="row justify-content-center">
								<div className="center mt-1">
									{submitted && this.props.login.loading && (
										<span className="error">{loginStatus}</span>
									)}
								</div>
							</div>
							<div className="row justify-content-center">
								<button
									type="submit"
									className="btn-flat col-5 btn-md w-100 text-uppercase"
									onClick={this.loginForm}
								>
									Se connecter
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

//REDUX------------------------------------------------
const mapStateToProps = (states: RootState, ownProps: OwnProps): StateProps => {
	return {
		login: states.login.result,
	};
};

const mapDispatchToProps = (
	dispatch: ThunkDispatch<{}, {}, any>,
	ownProps: OwnProps
): DispatchProps => {
	return {
		loginAction: (username, password) => dispatch(loginAction(username, password)),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
