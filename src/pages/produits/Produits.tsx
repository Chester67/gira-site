import * as React from "react";
import CountryCard from "../../components/produits/CountryCard";
import Pagination from "../../components/produits/Pagination";
import { getStore } from "../../utils/localStorage";
import AsyncSelect from "react-select/async";
import "./Produits.css";
import { ProduitService } from "../../services/produit-service";

interface TypeOption {
	readonly id: any;
	readonly nom: string;
	readonly code: string;
}

export class Produits extends React.Component<any, any> {
	private globalPageLimit = 9;
	private globalPageNeighbours = 1;

	constructor(props: any) {
		super(props);
		this.state = {
			value: "",
			currentProduits: [],
			currentPage: 1,
			totalPages: 1,
			totalSize: 0,
			pageErreur: null,
		};
	}

	public componentDidMount() {
		const user = getStore("user");
		console.log("here is now => " + JSON.stringify(user));
		if (user && user.token) {
			// this.apiTypeProduits(1).then((types) => {
			// 	if (types) {
			// 		console.log("types => " + JSON.stringify(types));
			// 	}
			// });
			this.apiProduitsPagination(1, this.globalPageLimit).then((produits) => {
				if (produits) {
					const totalSize = produits.totalSize;
					if (totalSize >= 0) {
						console.log("here is now");
						this.setState({ totalSize });
						console.log("total size => " + totalSize);
					}
				}
			});
		}
	}

	private apiProduitsPagination = async (currentPage: number, pageLimit: number) => {
		try {
			return ProduitService.apiProduitsPagination(currentPage, pageLimit);
		} catch (error) {
			this.setState({ pageErreur: "Erreur lors du chargement des produits" });
			return null;
		}
	};

	private apiTypeProduits = async (categorie: number) => {
		try {
			return ProduitService.apiTypeProduits(categorie);
		} catch (error) {
			return null;
		}
	};

	onPageChanged = (data: any) => {
		const { currentPage, totalPages, pageLimit } = data;
		if (currentPage >= 1) {
			this.apiProduitsPagination(currentPage, pageLimit).then((produits) => {
				console.log("produits => " + JSON.stringify(produits));
				if (produits) {
					const currentProduits = produits.result;
					const totalSize = produits.totalSize;
					this.setState({
						currentPage,
						currentProduits,
						totalPages,
						totalSize,
					});
				}
			});
		}
	};

	private promiseOptions = () =>
		new Promise<TypeOption[]>((resolve) => {
			this.apiTypeProduits(1).then((types) => {
				if (types) {
					console.log("types => " + JSON.stringify(types));
					resolve(types);
				}
			});
		});

	public render() {
		const { currentProduits, currentPage, totalPages, totalSize, pageErreur } =
			this.state;

		console.log("this is total size => " + totalSize);
		if (totalSize === 0) return null;

		return (
			<div className="container">
				<div className="row col-12 pl-0 pr-0 header-height mt-5">
					<div className="col-6 country-card-header flex-column h-100">
						<div className="col">
							<h4 style={{ marginTop: 10 }}>Type(s) de produit</h4>
						</div>
						<div className="mt-0 pt-0">
							<AsyncSelect
								className="col-11 mt-0 pt-0"
								isMulti
								cacheOptions
								defaultOptions
								loadOptions={this.promiseOptions}
								getOptionValue={(option) => option.id}
								getOptionLabel={(option) => option.nom}
							/>
						</div>
						<button type="button" className="btn btn-default btn-circle">
							<i className="fa fa-check"></i>
						</button>
					</div>
					<div className="col-6 d-flex flex-column pl-2 pr-0">
						<div className="col h-50 country-card-header pl-0 pr-0">
							Flex item inside col-sm-6
						</div>
						<div className="d-flex h-50 flex-row mt-2 pl-0 pr-0">
							<div className="col-10 country-card-header">
								Flex item inside col-sm-5
							</div>
							<div className="col-2 country-card-header ml-2">
								Flex item inside col-sm-1
							</div>
						</div>
					</div>
				</div>
				<div className="mb-5">
					<div className="row d-flex flex-row py-2">
						<div className="w-100 px-4 py-2 d-flex flex-row flex-wrap align-items-center justify-content-between">
							<div className="d-flex flex-row align-items-center">
								<h2>
									<strong className="text-secondary">
										{totalSize}
									</strong>{" "}
									Produits
								</h2>
								{currentPage && (
									<span className="current-page d-inline-block h-100 pl-4 text-secondary">
										Page{" "}
										<span className="font-weight-bold">
											{currentPage}
										</span>{" "}
										/{" "}
										<span className="font-weight-bold">
											{totalPages}
										</span>
									</span>
								)}
							</div>
							<div className="d-flex flex-row py-4 align-items-center">
								<Pagination
									totalRecords={totalSize}
									pageLimit={this.globalPageLimit}
									pageNeighbours={this.globalPageNeighbours}
									onPageChanged={this.onPageChanged}
								/>
							</div>
						</div>
						{currentProduits && currentProduits.length > 0 ? (
							currentProduits.map((produit: any) => (
								<CountryCard key={produit.id} country={produit} />
							))
						) : (
							<div>Aucun enregistrement</div>
						)}
					</div>
				</div>
			</div>
		);
	}
}
