import React from "react";
import routes from "../../routes";
import { Switch, Route } from "react-router-dom";
import Sidebar from "../../components/Sidebar/Sidebar";

class Acceuil extends React.Component<any, any> {
	private mainPanel: any;
	constructor(prop: any) {
		super(prop);
		this.mainPanel = React.createRef();
		this.state = {};
	}

	private getRoutes = (routes: any[]) => {
		return routes.map((prop, key) => {
			if (prop.layout === "/admin") {
				return <prop.component />;
			} else {
				return null;
			}
		});
	};

	render() {
		return (
			<div>
				<div>this is a test</div>
			</div>
		);
	}
}

export default Acceuil;
