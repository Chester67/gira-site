export interface Offer {
	id: string;
	titre: string;
	description: string;
}