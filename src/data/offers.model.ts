export interface Offers {
	id?: string;
	titre?: string;
	description?: string;
	prerequis?: Array<string>;
	role?: Array<string>;
	techno?: Array<string>;
	demarrage?: string;
}
