export interface Actu {
	id: string;
	image: string;
	titre?: string;
	description: string;
}