import hadoop from "./hadoop.png";
import react from "./react.png";

import angular from "./angular.png";
import elk from "./elk.png";
import mongo from "./mongo.png";
import hbase from "./hbase.png";
import scala from "./scala.png";
import android from "./android.png";
import ios from "./ios.png";
import java from "./java.png";
import net from "./net.png";
import php from "./php.png";
import swift from "./swift.png";
import flutter from "./flutter.png";
import kotlin from "./kotlin.png";

const technos: { [key: string]: string } = {
	hadoop,
	react,
	scala,
	angular,
	elk,
	mongo,
	hbase,
	android,
	java,
	ios,
	net,
	php,
	swift,
	flutter,
	kotlin,
};

export default technos;
