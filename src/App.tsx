import "./App.css";
import "./assets/css/light-bootstrap-dashboard-react.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import { Switch, Route } from "react-router-dom";
import AuthorizedRoute from "./components/route/AuthorizedRouteHOC";
import Sidebar from "./components/Sidebar/Sidebar";
import AdminNavbar from "./components/Navbars/AdminNavbar";
import routes from "./routes";
import React from "react";
import { connect } from "react-redux";
import { RootState } from "./store";
import { LoginResult } from "./store/login/reducers";
import { withRouter } from "react-router-dom";
import Login from "./pages/login/Login";
import { AppModal } from "./components/AppModal/AppModal";
interface StateProps {
	login: LoginResult;
}

class App extends React.Component<any, any> {
	private mainPanel: any;
	constructor(prop: any) {
		super(prop);
		this.mainPanel = React.createRef();
		this.state = {
			showLogoutModal: false,
		};
	}

	private checkRouteLogin = (location: { pathname: string | string[] }): boolean => {
		if (location && location.pathname) {
			if (location.pathname.includes("/register") || location.pathname === "/") {
				return true;
			}
		}
		return false;
	};

	private getRoutes = (routes: any[]) => {
		return routes.map((prop, key) => {
			if (prop.layout === "/admin") {
				return (
					<AuthorizedRoute
						path={prop.layout + prop.path}
						render={(props) => <prop.component {...props} />}
						key={key}
					/>
				);
			} else {
				return <Route exact path="/" component={Login} />;
			}
		});
	};

	private logoutUser = () => {
		console.log("onLogoutUser");
		this.setState({ showLogoutModal: true });
		setTimeout(() => {
			this.setState({ showLogoutModal: false });
			this.props.history.push("/");
		}, 3000);
	};

	render() {
		const { location } = this.props;
		return (
			<div className="wrapper container-back">
				{!this.checkRouteLogin(location) ? (
					<Sidebar color={"black"} image={""} routes={routes} />
				) : null}
				<div
					className={!this.checkRouteLogin(location) ? "main-panel" : ""}
					ref={this.mainPanel}
				>
					{!this.checkRouteLogin(location) ? (
						<AdminNavbar onLogoutUser={() => this.logoutUser()} />
					) : null}
					<div className="content">{this.getRoutes(routes)}</div>
				</div>
				<AppModal
					show={this.state.showLogoutModal}
					title={"DECONNEXION"}
					children={"Deconnexion en cours ..."}
				/>
			</div>
		);
	}
}

//REDUX------------------------------------------------
const mapStateToProps = (states: RootState, ownProps: any): StateProps => {
	return {
		login: states.login.result,
	};
};

export default withRouter(connect(mapStateToProps)(App));
