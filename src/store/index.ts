import { createStore, combineReducers, applyMiddleware } from 'redux'
import login, { State as LoginState } from './login/reducers'
import session, { State as SessionState } from './session/reducers'
import thunk from 'redux-thunk'

export interface RootState {
  session: SessionState,
  login: LoginState
}

export default createStore(combineReducers<RootState>({
  session,
  login
}), applyMiddleware(thunk))