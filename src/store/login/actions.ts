/* eslint-disable @typescript-eslint/no-unused-expressions */
import {
	FETCHING_LOGIN_BEGIN,
	FETCHING_LOGIN_FAILURE,
	FETCHING_LOGIN_SUCCESS,
} from "./types";
import { LOGIN_API } from "../../utils/apiUrls";
import { AnyAction } from "redux";
import { ThunkDispatch } from "redux-thunk";

// Action Definition
export interface SetAction {
  type: string;
  payload: any;
}

// Union Action Types
export type Action = SetAction;

// Action Creators
export const fetchLoginBegin = (): SetAction => {
  return { 
		type: FETCHING_LOGIN_BEGIN,
		payload: null, 
	}
}

export const fetchLoginSuccess = (success: any): SetAction => {
	return {
		type: FETCHING_LOGIN_SUCCESS,
		payload: success,
	}
}

export const fetchLoginFailure = (error: any): SetAction => {
	return {
		type: FETCHING_LOGIN_FAILURE,
		payload: error,
	}
}

// thunk action
export const loginAction = (email: string, password: string) =>
 (dispatch: ThunkDispatch<{}, {}, AnyAction>) => {
		dispatch(fetchLoginBegin());
		return fetch(LOGIN_API, {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({ email: email, password: password }),
		})
		.then((res) => res.json())
		.then((json) => {
			console.log(`RESPONSE LOGIN => ${JSON.stringify(json)}`);
			setTimeout(() => {
				dispatch(fetchLoginSuccess(json));
			}, 3000);
			
		})
		.catch((e) => {
			console.log("ERROR LOGIN => " + e);
			dispatch(fetchLoginFailure(e));
		});
	};

// thunk action
/*export const loginAction = (username: string, password: string): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
  // Invoke API
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
    return new Promise<void>((resolve) => {
      dispatch(fetchLoginBegin());
			setTimeout(() => {
				dispatch(fetchLoginSuccess("test encore"));
				resolve()
			}, 3000);
    })
  }
}*/

	

