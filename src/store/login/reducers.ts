// store/session/reducers.ts
import { combineReducers } from 'redux'
import { Action } from './actions'
import { FETCHING_LOGIN_BEGIN, FETCHING_LOGIN_FAILURE, FETCHING_LOGIN_SUCCESS } from './types'

//------------------------------
// States' definition
export interface LoginResult {
  response: any
  loading: boolean
  error: any
}

const initialState: LoginResult = {
  response: {},
  loading: false,
  error: null,
};

export interface State {
  result: LoginResult
}

const result = (state: LoginResult = initialState, action: Action): LoginResult  => {
  switch (action.type) {
  case FETCHING_LOGIN_BEGIN:
    return {...state, loading: true, error: null}
  case FETCHING_LOGIN_FAILURE:
    return {...state, loading: false, error: action.payload}
  case FETCHING_LOGIN_SUCCESS:
    return {...state, loading: false, error: null, response: action.payload}
  }
  return state;
};

export default combineReducers<State>({
  result
})