const API_BASE_URL = 'http://api.supercompareur.fr/v1';

export const LOGIN_API = API_BASE_URL + '/utilisateur/login';
export const PRODUITS_API = API_BASE_URL + '/produits/pagination';
export const PRODUITS_TYPES_API = API_BASE_URL + '/types/findByFilter';
export const CREATE_USER_API = API_BASE_URL + '/newuserfront';
export const ENTREPRISE_API = API_BASE_URL + '/listacteur';

export const LINK_CGU = 'https://bo.manifestations-ne.ch/fvd/cgu';
export const LINK_MENTION = 'https://bo.manifestations-ne.ch/fvd/mentionlegale';
export const LINK_PROPOS = 'https://bo.manifestations-ne.ch/fvd/apropos';
