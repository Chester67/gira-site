
import Magasins from "./pages/magasins/Magasins";
import { Produits } from "./pages/produits/Produits";
import Login from "./pages/login/Login";

const dashboardRoutes = [
  {
    upgrade: true,
    path: "/upgrade",
    name: "Upgrade to PRO",
    icon: "nc-icon nc-alien-33",
    component: Magasins,
    layout: "/admin",
  },
  {
    path: "/",
    name: "Login",
    icon: "nc-icon nc-chart-pie-35",
    component: Login,
    layout: "",
  },
  {
    path: "/produits",
    name: "Produits",
    icon: "nc-icon nc-chart-pie-35",
    component: Produits,
    layout: "/admin",
  },
  {
    path: "/magasins",
    name: "Magasins",
    icon: "nc-icon nc-circle-09",
    component: Magasins,
    layout: "/admin",
  },
];

export default dashboardRoutes;
